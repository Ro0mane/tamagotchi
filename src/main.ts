const infofaim = ['0faim.png', '1faim.png', '2faim.png', '3faim.png', '4faim.png', '5faim.png']
const infojoie = ['0joie.png', '1joie.png', '2joie.png', '3joie.png', '4joie.png', '5joie.png']
const infofood = ['0food.png', '1food.png', '2food.png', '3food.png', '4food.png', '5food.png']

const tamagotchiimg = document.querySelector<HTMLImageElement>("#tamagotchi")
const jouerButton = document.querySelector<HTMLButtonElement>("#Jouer");
const nourriButton = document.querySelector<HTMLButtonElement>("#nourri");
const cuisinerButton = document.querySelector<HTMLButtonElement>("#cuisiner");
const hapinessimg = document.querySelector<HTMLImageElement>("#hapiness");
const faimimg = document.querySelector<HTMLImageElement>("#faim");
const foodimg = document.querySelector<HTMLImageElement>("#food");

let faim = 3
let joie = 3
let food = 0
Hungry()
Happy()
ManageButtons()

faimimg.src = infofaim[faim]
hapinessimg.src = infojoie[joie]
foodimg.src = infofood[food]

//augmente la joie,  une image d'un tamagotchi qui cuisine apparait pendant 5 secondes
jouerButton.addEventListener('click', () => {
  joie++;
  hapinessimg.src = infojoie[joie];
  tamagotchiimg.src = "heureux.png";
  setTimeout(() => {
    tamagotchiimg.src = "normal.png";
  }, 5000);
});

//augmente la nouriture,  une image d'un tamagotchi qui cuisine apparait pendant 5 secondes
cuisinerButton.addEventListener('click', () => {
  food++;
  foodimg.src = infofood[food];
  tamagotchiimg.src = "cuisto.png";
  setTimeout(() => {
    tamagotchiimg.src = "normal.png";
  }, 5000);
});

//bouton pour le nourrir, fait baisser la nourriture et augmente la jogge de faim, une image d'un tamagotchi qui mange apparait pendant 5 secondes
nourriButton.addEventListener('click', () => {
  faim++;
  food--;
  faimimg.src = infofaim[faim];
  foodimg.src = infofood[food];
  tamagotchiimg.src = "mange.png";
  setTimeout(() => {
    tamagotchiimg.src = "normal.png";
  }, 5000);
});

//fonction qui gère la baisse de la faim, le tamagotchi a un peu plus faim toute les 30 secondes
function Hungry() {
  setTimeout(() => {
    if (faim > 0) {
      faim--;
      faimimg.src = infofaim[faim];
      nourriButton.disabled = false;
      Hungry()
    }
  }, 30000)
};

// fonction qui donne au tamagotchi un peu plus envie de jouer toutes les 30 secondes, si la barre de joie est à 0 il meurt
function Happy() {
  setTimeout(() => {

    if (joie > 0) {
      joie--
      hapinessimg.src = infojoie[joie]
      Happy();
    }
    if (joie < 5) {
      jouerButton.disabled = false;
    }
    if (joie == 0) {
      showPopupdead();
    }
  }, 30000)
};

//fonction qui gère l'apparition et disparition de tous les boutons, vérification très régulière
function ManageButtons() {
  setTimeout(() => {
    if (joie == 5) {
      jouerButton.disabled = true;
    }
    if (food == 5) {
      cuisinerButton.disabled = true;
    }
    if (food > 0) {
      nourriButton.disabled = false;
      console.log("food > 0");
    }
    if (food == 0) {
      nourriButton.disabled = true;
    }
    if (food < 5) {
      cuisinerButton.disabled = false;
    }
    if (faim == 5) {
      nourriButton.disabled = true;
    }
    if (faim == 0) {
      showPopupdead()
      console.log("dead");
    }
    ManageButtons()
  }, 100)
}

//fonction qui fait apparaitre le message de fin quand la jogge de faim ou de joie est vide, un bouton recharge la page pour reccomencer le jeu
function showPopupdead() {
  const finalMessage: string = `Je suis mort, moi qui ne voulais que manger et m'amuser, tu n'as même pas réussi à m'aider...`;

  const popupElement: HTMLDivElement = document.createElement('div');
  popupElement.classList.add('popup');

  const messageElement: HTMLParagraphElement = document.createElement('p');
  messageElement.textContent = finalMessage;

  const restartButton: HTMLButtonElement = document.createElement('button');
  restartButton.textContent = 'Recommencer';

  restartButton.addEventListener('click', () => {
    location.reload();
  });

  popupElement.appendChild(messageElement);
  popupElement.appendChild(restartButton);

  document.body.innerHTML = '';
  document.body.appendChild(popupElement);
};